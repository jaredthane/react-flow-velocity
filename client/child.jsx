Child = React.createClass({
  // componentWillMount(){
  //   console.log('Will Mount');
  //   this.style={
  //     position:'absolute',
  //     top:0,
  //     bottom:'auto',
  //     width:'100%',
  //   };
    
  //   console.log("this.style: " + JSON.stringify(this.style, null, 2));
  // },
	componentWillEnter(done){
		console.log('Will Enter');
		node = this.getDOMNode();
    $(node).velocity({
      left: [0,'100%'],
    }, {
      // duration: 5000,
      complete: done,
    });
	},
  // componentWillAppear(done){
  //   // this.didAppear = true;
  //   console.log('Will Appear');
  //   this.style.left = "0";
  //   // console.log("this.style: " + JSON.stringify(this.style, null, 2));
  //   done();
  // },
  // componentDidAppear(done){
  //   // this.didAppear = true;
  //   console.log('Did Appear');
  //   // this.style.left = "0";
  //   // console.log("this.style: " + JSON.stringify(this.style, null, 2));
  //   done();
  // },
	componentWillLeave(done){
		console.log('Will Leave');
		node = this.getDOMNode();
    $(node).velocity({
      // duration: 5000,
      left: ['-100%',0],
    }, {
      complete: done,
    });
	},
  render(){
    style={
      position:'absolute',
      top:0,
      bottom:'auto',
      width:'100%',
    };
    return (
      <div style={style}>
        {this.props.children}
      </div>
    )
  },
});
