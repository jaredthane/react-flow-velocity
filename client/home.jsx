Views.Home = React.createClass({
  render(){
    const style={
      width:'100%', 
      minHeight:700, 
      backgroundColor:'#46a1de', 
      color:'white',
    };
    return (
      <div id="home" key="home" style={style}>
        Home
        <a href="/articles" >Go To Articles</a>
      </div>
    )
  }
});



// Views.HomePage = React.createClass({
//   render(){
//     return (
//       <div id="home" key="home" style={{width:'100%', minHeight:700, backgroundColor:'#46a1de', color:'white'}}>
//         Home
//         <a href="/articles" >Go To Articles</a>
//       </div>
//     )
//   }
// });


