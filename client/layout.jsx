ReactTransitionGroup = React.addons.TransitionGroup;
Views.MainLayout = React.createClass({
  mixins: [ReactMeteorData],
  getMeteorData() {
    return {
      menuOpen: Session.get('menuOpen'),
      routeName: Session.get('routeName'),
    }
  },
  render(){
    const styles={
      contentContainer:{
        // position: 'absolute',
        // left:0,
        // right:0,
      },
      mainLayout:{
        transition:'left 0.5s',
        position: 'absolute',
        left:0,
        right:0,
      }
    };
    if (this.data.menuOpen){
      styles.mainLayout.left = 300;
    }
    return (
      <div style={{background:'grey', width:'100%', height:780}}>
        <Views.Menu />
        <div id="mainLayout" style={styles.mainLayout}>
          {this.props.header}
          <div id="content-container" style={styles.contentContainer}>
            <ReactTransitionGroup id="react-transition-group" style={{width:'100%'}}>
              <Child key={this.data.routeName}>
                {this.props.content}
              </Child>
            </ReactTransitionGroup>
          </div>
          {this.props.footer}
        </div>
      </div>
    )
  },
});