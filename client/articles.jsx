Views.Articles = React.createClass({
  render(){
    const style={
      width:'100%', 
      minHeight:700, 
      backgroundColor:'#2ecc71', 
      color:'white',
    };
    return (
      <div id="articles" key="articles" style={style}>
        Articles
        <a href="/" >Go Home</a>
      </div>
    )
  },
});
