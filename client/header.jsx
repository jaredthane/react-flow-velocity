Views.Header = Radium( React.createClass({
	toggleMenu(){
		Session.set('menuOpen', !Session.get('menuOpen'));
	},
  render(){
  	const styles = {
      container: {
      	color:'white',
      	zIndex: 10,
      	position: 'absolute',
      	backgroundColor:'green',
				top: 0,
				right: 0,
				bottom: 'auto',
				left: 0,
				width: '100%',
				height: 40,
      },
      menuButton: {
      	margin:'5px 20px', 
      	color:'#FFFFFF', 
      	cursor:'pointer',
      	display: 'inline-block',
      	transition:'color 0.5s',
      	':hover': {
            color: shadeBlendConvert(-0.2,'#FFFFFF'),
        },
      	fontSize:20
      },
    };

    return (
      <div style={styles.container}>
        <a style={styles.menuButton} onClick={this.toggleMenu}><i className="fa fa-bars"></i></a>
        Header
      </div>
    )
  },
}));
