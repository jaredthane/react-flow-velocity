FlowRouter.route("/",{
  name:'Home',
  action(params){
    Session.set('routeName','Home');
    renderMainLayoutWith(<Views.Home />);
  }
});
FlowRouter.route("/articles",{
  name:'Articles',
  action(params){
    Session.set('routeName','Articles');
    renderMainLayoutWith(<Views.Articles />);
  }
});
// FlowRouter.route("/todo",{
//   name:'Todo',
//   action(params){

//   	React.render(<TodoList />, document.getElementsByTagName("body")[0]);
    
//   }
// });
function renderMainLayoutWith(component){
  ReactLayout.render(Views.MainLayout, {
    header:<Views.Header />,
    content:component,
    footer:<Views.Footer />,
  });
}


    